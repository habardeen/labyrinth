import keras as K
import numpy as np
import math
import tensorflow as tf
from keras.layers import Dense, Input, Conv2D, MaxPooling2D, Flatten, Conv2DTranspose, UpSampling2D, Cropping2D, Reshape
from keras.optimizers import Adam
from keras.backend import set_session

epochs = 10
learn_rate = 0.001            # Default: 0.001
training_split = 0.8
batch_size = 1
model_name = 'model.h5'
loss = 'mse'


def pad(array, max_size, constant):
    padding = max_size - array.shape[0]
    return np.pad(array, (padding, 0), 'constant', constant_values=constant)


def data_generator(dir_, X_name, Y_name, max_size, samples, training = True):
    while 1:

        if training:
            np.random.shuffle(samples)

        for i, num in enumerate(samples):
            X = np.load(dir_ + X_name + str(num) + '.npy').astype(np.int)
            Y = np.load(dir_ + Y_name + str(num) + '.npy').astype(np.float32)
            #X = pad(X, max_size, 0)
            #Y = pad(Y, max_size, 0.)
            X = np.expand_dims(X, axis=0)
            Y = np.expand_dims(Y, axis=0)
            # X = np.expand_dims([X], axis=3)
            # Y = np.expand_dims([Y], axis=3)
            yield X, Y

def train_neural(dir_, X_name, Y_name, max_size, num_samples):

    # config = tf.ConfigProto(intra_op_parallelism_threads=8,
    #                         inter_op_parallelism_threads=8, allow_soft_placement=True,
    #                         device_count={'CPU': 1, 'GPU': 0})
    # sess = tf.Session(config=config)
    # set_session(sess)

    #inp = Input(shape=(max_size, max_size, 1))
    # down1 = Conv2D(32, 2, activation='relu')(inp)
    # down1 = MaxPooling2D(pool_size=(2, 2))(down1)
    # down2 = Conv2D(64, 5, activation='relu')(down1)
    # down2 = MaxPooling2D(pool_size=(2, 2))(down2)
    # up1 = Conv2DTranspose(64, 5, activation='relu')(down2)
    # up1 = UpSampling2D(size=(2,2))(up1)
    # up2 = Conv2DTranspose(32, 2, activation='relu')(up1)
    # up2 = UpSampling2D(size=(2,2))(up2)
    # crop = Cropping2D(cropping=((3, 3), (3, 3)))(up2)
    # out = Conv2D(1, 1, activation='softmax')(crop)

    inp = Input(shape=(max_size, max_size))
    hidden1 = Dense(int(max_size/2), activation='relu')(inp)
    hidden2 = Dense(max_size, activation='softmax')(hidden1)
    out = Reshape((max_size, max_size))(hidden2)
    model = K.Model(inputs=inp, outputs=out)

    optim = Adam(lr=learn_rate)
    model.compile(optimizer=optim, loss=loss)

    split = int(num_samples * training_split)
    samples = np.arange(0, num_samples)
    np.random.shuffle(samples)

    train_samples = samples[:split]
    tr_steps = math.ceil(train_samples.size / batch_size)
    val_samples = samples[split:]
    val_steps = math.ceil(val_samples.size / batch_size)

    generator_tr = data_generator(dir_, X_name, Y_name, max_size, train_samples)
    generator_val = data_generator(dir_, X_name, Y_name, max_size, val_samples, training = False)

    model.fit_generator(generator_tr, validation_data=generator_val, epochs=epochs,
                        use_multiprocessing=True, shuffle=True, steps_per_epoch=tr_steps,
                        validation_steps=val_steps)
    model.save(model_name)


def overlay(R, Q):
    for i, R_room in enumerate(R):
        valid_doors = np.nonzero(R_room)[0]
        valid_doors.tolist()
        #print(valid_doors)
        for k, Q_door in enumerate(Q[i]):
            if k not in valid_doors:
                Q[i][k] = 0.0
    return Q

def test_neural(R, max_size, tries):

    R_shape = R.shape[0]
    initial = np.random.randint(0, R_shape - 1)
    stop = [R_shape - 1]
    track = 0

    print()
    print('Starting Test from room: ' + str(initial))
    print('Labyrinth size: ' + str(R_shape) + ' rooms, ' + str(np.count_nonzero(R[-1])) + ' exits')

    #R = pad(R, max_size, 0)
    model = K.models.load_model(model_name)
    Q = model.predict(np.array([R]))
    Q = np.squeeze(Q, axis=0)
    #Q = Q[max_size - R_shape:, max_size - R_shape:]

    Q = overlay(R, Q)

    print(Q)

    best_path = str(initial)

    while initial not in stop:
        track += 1
        pre_decision = initial
        initial = np.argmax(Q[initial])

        # Do not go back to the previous room
        Q[initial][pre_decision] = 0.0

        best_path = best_path + ' -> ' + str(initial)
        if track > Q.shape[0]*tries:
            print(Q.shape[0])
            print(best_path)
            print('Test Failed in room #' + str(initial))
            return Q

    print()
    print('AGENT OPTIMAL PATH: ' + best_path)
    return Q