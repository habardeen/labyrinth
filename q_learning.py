import numpy as np
# from neural import train_neural, test_neural
from datetime import datetime
from labyrinth import generate_labyrinth

################
#   SETTINGS   #
################

preprocessing = False                   # Get labyrinth ground truth
testing = False                         # Process preproc_num tests on data files
data_dir = './data/'                    # Data directory
error_dir = data_dir + 'invalid/'       # Invalid labyrinths directory
Q_file = 'Q_labyr_'                     # .npy for Q
R_file = 'Labyr_'                       # .npy for R
preproc_num = 200                       # Num of labyrinths to create and train on

# Labyrinths
min_rooms = 20
max_rooms = 1000
max_exits = 5

# Q-Learning
alpha = 0.7                             # Learning rate - how fast you are training
gamma = 0.6                             # Discount rate - how much future rewards are weighted
epsilon = 0.9                           # For random / greedy policy initial epsilon value
double_espilon = False                  # If true, two epsilons used (see report Advanced techniques)
min_episodes = max_rooms / 3            # Do not stop before
max_episodes = 4000 #max_rooms * 10        # Early stop is done in find_exit
agent_tries = 100                       # Num of times the agent keeps on trying to find exit (multiple of R[0])
early_stop = True                       # Early stop ON /OFF
perf_points = 8                        # Points in time to measure algorithmic performance (multiple of 10)
stop_train = 10                         # Number of digits to check perf_avg for stopping

# Reporting and plotting
np.set_printoptions(precision=1, suppress=True)  # print every float only with 1 decimals (does not change their values
print_step_details = False              # for reporting about the steps of an episode
print_q_matrices = False

save_num_of_steps = False
num_of_steps_per_episode = []           # number of steps in an episode over all episodes

save_q_matrices = False
q_matrices_per_episodes = []            # Q-matrices for each episodes in a training

save_episodes_per_training = False
num_of_episodes_per_training = []       # number of episodes per training

save_diff_per_episodes = False
difference_per_episodes = []            # differences between variance and avg variance per episode


################
#   TRAINING   #
################

def get_actions(state, matrix):
    return np.where(matrix[state] > 0)[0]


def get_random(max_range):
    return np.random.randint(0, max_range)


def Q_update(old_q, reward, next_q):
    return old_q + alpha * (reward + gamma * np.amax(next_q) - old_q)


def epsilon_decay(epsilon_):
    if epsilon_ > 0.5:
        epsilon_ *= 0.9999
    else:
        epsilon_ *= 0.999
    return epsilon_


# Find labyrinth exit
def find_exit(state, R, Q, epsilon_):
    stop = [R.shape[0] - 1]
    track = 0

    internal_epsilon = epsilon_
    if print_step_details:
        print('========= Initial state: ' + str(state))
        print()

    # FIND EXIT
    while state not in stop:

        # Keep track of transitions
        track += 1

        # 1) Check available actions
        avail_actions = get_actions(state, R)

        # 2b) e-greedy policy
        rand = np.random.uniform(0, 1)
        internal_epsilon = epsilon_decay(internal_epsilon)
        if print_step_details:
            print('Epsilon: ' + str("{0:.2f}".format(epsilon)) + ' Random number: ' + str("{0:.2f}".format(rand)))
        if rand > internal_epsilon:
            # Exploit
            exploit = np.zeros(len(avail_actions))
            for i, action in enumerate(avail_actions):
                exploit[i] = Q[state][action]
            get_reward = np.argmax(exploit)
            next_state = avail_actions[get_reward]
            if print_step_details:
                print('Next state (Exploit): ' + str(next_state))
        else:
            # Explore
            next_state = avail_actions[get_random(len(avail_actions))]
            if print_step_details:
                print('Next state (Explore): ' + str(next_state))
        reward = R[state][next_state]

        if print_step_details:
            print ('Reward: ' + str(reward))

        # 3) Check Q(s,a) and Q(s',a')
        old_q = Q[state][next_state]
        next_actions = get_actions(next_state, R)
        next_q = np.zeros(len(next_actions))
        for i in range(next_q.size):
            next_q[i] = Q[next_state][next_actions[i]]

        # 4) Update Q(s,a) and state
        updated_q = Q_update(old_q, reward, next_q)
        Q[state][next_state] = updated_q
        state = next_state

        if print_step_details:
            print('Old q: ' + str(old_q) + ' Updated q: ' + str(updated_q))
            print('Q-matrix after update:')
            print(Q)

        # If transitions > const * number of rooms => Labyrinth is invalid
        if track > Q.shape[0] * agent_tries:
            print('Current state = ' + str(state))
            print('Next state = ' + str(next_state))
            if save_num_of_steps:
                num_of_steps_per_episode.append(track)
            return False

    if save_num_of_steps:
        num_of_steps_per_episode.append(track)
    return True


# Training
def train(state, R, Q=None):

    total = 0
    print(total)
    performance = np.zeros(perf_points)
    perf_avg = 0.0
    external_epsilon = epsilon

    if Q is None:
        Q = np.zeros(R.shape)

    print(Q.shape)

    # EPISODES
    for episode in range(0, max_episodes):

        # Tracking performance
        start = datetime.now()
        old_perf_avg = perf_avg

        # Train one episode
        valid = find_exit(state, R, Q, external_epsilon)
        if double_espilon:
            external_epsilon = epsilon_decay(external_epsilon)
        elapsed = datetime.now() - start
        total += elapsed.microseconds * 1000

        print()
        print('====> EPISODE: ' + str(episode))
        print('      Time: ' + str(elapsed.microseconds) + 'ms' + ' Variance (avg): ' + str(round(perf_avg, stop_train)))

        # Discarding labyrinth if training failed
        if not valid:
            print('Discarding Labyrinth #' + str(episode) + ': training too slow')
            return False

        # Break training on sampled variance threshhold (avg)
        performance[episode % perf_points] = np.var(Q)
        perf_avg = np.average(performance)

        if save_diff_per_episodes:
            difference = abs(round(perf_avg, stop_train) - round(old_perf_avg, stop_train))
            difference_per_episodes.append(difference)

        # If early stop is on, check the variances and stop training if it is close to zero
        if early_stop:
            if round(perf_avg, stop_train) == round(old_perf_avg, stop_train) and episode > min_episodes:
                print('Average performance not increased --> Stopped training')
                if save_episodes_per_training:
                    num_of_episodes_per_training.append(episode)
                return True

        # Initialise a new start for same labyrinth
        state = get_random(R.shape[0])

        # For reporting
        if save_q_matrices:
            q_matrices_per_episodes.append(Q)
        if print_q_matrices:
            print(Q)
    if save_episodes_per_training:
        num_of_episodes_per_training.append(max_episodes)
    print()
    print('TOTAL TIME: ' + str(int(total / 1000)) + 'ms')
    return True


#####################
#   PREPROCESSING   #
#####################

# Test a Q matrix
def test(Q, initial=None):
    if initial is None:
        initial = get_random(Q.shape[0] - 1)
    stop = [Q.shape[0] - 1]
    track = 0

    print()
    print('Starting Test from room: ' + str(initial))
    print('Labyrinth size: ' + str(Q.shape[0]) + ' rooms')

    best_path = str(initial)

    while initial not in stop:
        track += 1
        if track > Q.shape[0] * agent_tries:
            print('Test Failed in room #' + str(initial))
            return False
        pre_decision = initial
        initial = np.argmax(Q[initial])

        # Do not go back to the previous room
        Q[initial][pre_decision] = 0.

        best_path = best_path + ' -> ' + str(initial)

    print()
    print(Q.astype(int))
    print('AGENT OPTIMAL PATH: ' + best_path)
    return True


# Testing a labyrinth file
def test_file(test_num, invalid=False, initial=None):

    if invalid:
        Q_name = error_dir + R_file + str(test_num) + '_INVALID.npy'
    else:
        Q_name = data_dir + Q_file + str(test_num) + '.npy'

    Q = np.load(Q_name)
    print(Q.astype(int))

    valid = test(Q)

    if valid:
        return True
    else:
        return False


# Pretraining preproc_num labyrinths (saving to disk)
def preprocess(prep=preproc_num, resume=0):
    for preproc in range(resume, prep):

        print()
        print('################# LABYRINTH #' + str(preproc))

        # Generate labyrinth
        tot_rooms = max_rooms  # np.random.randint(min_rooms, max_rooms)
        tot_exits = np.random.randint(1, max_exits)
        R = generate_labyrinth(tot_rooms, tot_exits)

        print(R)

        # Get Q
        state = get_random(R.shape[0] - 1)
        Q = np.zeros(R.shape)
        valid = train(state, R, Q)

        if not valid:
            R_name = error_dir + R_file + str(preproc) + '_INVALID'
            Q_name = error_dir + Q_file + str(preproc) + '_INVALID'
        else:
            R_name = data_dir + R_file + str(preproc)
            Q_name = data_dir + Q_file + str(preproc)
        np.save(R_name, R.astype(np.int8))
        np.save(Q_name, Q.astype(np.float16))

        if not valid:
            preprocess(resume=preproc)
            break
        else:
            test_file(preproc)
            print()
            print('Labyrinth #' + str(preproc) + ' has been vanquished!')


################
#     MAIN     #
################

# valid_ = False
# while valid_ is not True:
#     valid_ = train(np.random.randint(0, max_rooms), generate_labyrinth(max_rooms, 1))


# NEURAL
# preprocess(preproc_num)

# if testing:
#     tests = []
#     for i in range(0, preproc_num):
#         valid_ = test_file(i)
#         tests.append(valid_)
#     print()
#     print('Are all labyrinths valid? Answer: ' + str(all(tests)))

#train_neural(data_dir, R_file, Q_file, max_rooms, preproc_num)

# lab = generate_labyrinth(max_rooms, max_exits)
# Q_ = test_neural(lab, max_rooms, agent_tries)
# test(Q_)
