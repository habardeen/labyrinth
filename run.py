import numpy
import q_learning as ql
import numpy as np
from labyrinth import generate_labyrinth
import matplotlib.pyplot as plt


# Reporting script with plots or console messages. Uncomment one report to run.


                                       #################
                                       # BASIC REPORTS #
                                       #################

###########
#   1.8   #
###########
# Fixed labyrinth to show Q-matrix updates
# R = np.array([(  0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
#                 (1, 0, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
#                 (0, 1, 0, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
#                 (0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
#                 (0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0),
#                 (0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
#                 (0, 0, 1, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0),
#                 (0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0),
#                 (0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 100),
#                 (0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 0),
#                 (0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0),
#                 (0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0),
#                 (0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0),
#                 (0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 1, 0, 0),
#                 (0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0),
#                 (0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 100),
#                 (0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 100)])
# Q = np.zeros(R.shape)

# ql.alpha = 0.7                             # Learning rate - how fast you are training
# ql.gamma = 0.6                             # Discount rate - how much future rewards are weighted
# ql.epsilon = 0.9                           # For random / greedy policy, resets for each labyrinth
# ql.max_episodes = 1                        # Max episodes
# ql.print_step_details = True

# ql.train(0, R, Q)
#-----------------------------------------------------------------------------------------------------------------------


############
#   1.9    #
############
# Report 1.9/1 - Fixed labyrinth to show Q-matrices after each episode

# R = np.array([(  0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
#                 (1, 0, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
#                 (0, 1, 0, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
#                 (0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
#                 (0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0),
#                 (0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
#                 (0, 0, 1, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0),
#                 (0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0),
#                 (0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 100),
#                 (0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 0),
#                 (0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0),
#                 (0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0),
#                 (0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0),
#                 (0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 1, 0, 0),
#                 (0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0),
#                 (0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 100),
#                 (0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 100)])
# Q = np.zeros(R.shape)
#
# ql.alpha = 0.7                             # Learning rate - how fast you are training
# ql.gamma = 0.6                             # Discount rate - how much future rewards are weighted
# ql.epsilon = 0.9                           # For random / greedy policy, resets for each labyrinth
# ql.max_episodes = 10                       # Max episodes
# ql.print_q_matrices = True
# ql.save_q_matrices = False
# ql.train(0, R, Q)
#-----------------------------------------------------------------------------------------------------------------------

# Report 1.9/2 - Random labyrinth to show the necessary steps in each episodes
# R = generate_labyrinth(50, 2)
# ql.early_stop = False
# Q = np.zeros(R.shape)
# ql.alpha = 0.7                             # Learning rate - how fast you are training
# ql.gamma = 0.1                             # Discount rate - how much future rewards are weighted
# ql.epsilon = 0.9                           # For random / greedy policy
# ql.min_episodes = 1                        # Do not stop before
# ql.max_episodes = 150                      # Max episodes
#
# ql.save_num_of_steps = True
# ql.train(0, R, Q)
#
# # plot performance
# stps = np.arange(0,len(ql.num_of_steps_per_episode), 1)
# plt.plot(stps, ql.num_of_steps_per_episode,                       # plot with continuous line, 0.5 wide red line
#            '-', lw=0.5, c='red', aa=True)
# plt.xticks(np.arange(0,160,10))                                   # put ticks in Y at every 10 between 0:160
#
# plt.title('50 rooms, 150 episodes, Epsilon: 0.9, Alpha 0.7, Gamma: 0.1')   # set title
# plt.xlabel('Episodes')                                            # set x label
# plt.ylabel('Steps')                                               # set y label
# plt.show()                                                        # show plot
#-----------------------------------------------------------------------------------------------------------------------

#############
#   1.10    #
#############

# Report 1.10/1 -  Show Q-matrix with high epsilon on fixed labyrinth
# R = np.array([(  0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
#                 (1, 0, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
#                 (0, 1, 0, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
#                 (0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
#                 (0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0),
#                 (0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
#                 (0, 0, 1, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0),
#                 (0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0),
#                 (0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 100),
#                 (0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 0),
#                 (0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0),
#                 (0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0),
#                 (0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0),
#                 (0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 1, 0, 0),
#                 (0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0),
#                 (0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 100),
#                 (0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 100)])
# Q = np.zeros(R.shape)
#
# ql.alpha = 0.7                             # Learning rate - how fast you are training
# ql.gamma = 0.6                             # Discount rate - how much future rewards are weighted
# ql.epsilon = 0.95                          # For random / greedy policy
# ql.max_episodes = 10                       # Max episodes
# ql.print_q_matrices = True
# ql.save_q_matrices = True
# ql.train(0, R, Q)
#-----------------------------------------------------------------------------------------------------------------------

# Report 1.10/2 - Lower learning rate to show Q-matrix
# R = np.array([(  0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
#                 (1, 0, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
#                 (0, 1, 0, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
#                 (0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
#                 (0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0),
#                 (0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
#                 (0, 0, 1, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0),
#                 (0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0),
#                 (0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 100),
#                 (0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 0),
#                 (0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0),
#                 (0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0),
#                 (0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0),
#                 (0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 1, 0, 0),
#                 (0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0),
#                 (0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 100),
#                 (0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 100)])
# Q = np.zeros(R.shape)
#
# ql.alpha = 0.2                             # Learning rate - how fast you are training
# ql.gamma = 0.6                             # Discount rate - how much future rewards are weighted
# ql.epsilon = 0.9                          # For random / greedy policy
# ql.max_episodes = 10                       # Max episodes
# ql.print_q_matrices = True
# ql.save_q_matrices = True
# ql.train(0, R, Q)
#-----------------------------------------------------------------------------------------------------------------------

# Report 1.10/3 - Lower gamma to show Q-matrix
# R = np.array([(  0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
#                 (1, 0, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
#                 (0, 1, 0, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
#                 (0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
#                 (0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0),
#                 (0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
#                 (0, 0, 1, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0),
#                 (0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0),
#                 (0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 100),
#                 (0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 0),
#                 (0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0),
#                 (0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0),
#                 (0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0),
#                 (0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 1, 0, 0),
#                 (0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0),
#                 (0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 100),
#                 (0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 100)])
# Q = np.zeros(R.shape)
#
# ql.alpha = 0.7                             # Learning rate - how fast you are training
# ql.gamma = 0.2                             # Discount rate - how much future rewards are weighted
# ql.epsilon = 0.9                          # For random / greedy policy
# ql.max_episodes = 10                       # Max episodes
# ql.print_q_matrices = True
# ql.save_q_matrices = True
# ql.train(0, R, Q)
#-----------------------------------------------------------------------------------------------------------------------

                                                ####################
                                                # ADVANCED REPORTS #
                                                ####################

# These reports are to find the optimal parameters for reaching early stops during learning. By changing the parameters
# alpha and gamma, we observe how soon early stop happens

# Report 1 - Run training and plot the results with 20 alpha values on 100 / 1000 rooms labyrinth

# ql.max_rooms = 1000
# R = generate_labyrinth(ql.max_rooms, 2)
#
# ql.save_episodes_per_training = True
#
# for i in range (1,21):
#     Q = np.zeros(R.shape)
#     ql.alpha = 0.05 * i
#     ql.train(0, R, Q)
#
# #plot performance
# alphas = np.arange(0.05, 1.05, 0.05)                            # Alpha values for Y axis
# plt.plot(ql.num_of_episodes_per_training,
#          alphas, '.', lw=0.5, c='red', aa=True)                 # plot with continuous line, 0.5 wide red line
# plt.yticks(alphas)                                              # put ticks in Y at every alpha value
#
# plt.title('100 rooms, Gamma = 0.6, Epsilon = 0.9')              # set title
# plt.xlabel('Episodes')                                          # set x label
# plt.ylabel('Alpha')                                             # set y label
# plt.show()                                                      # show plot
#-----------------------------------------------------------------------------------------------------------------------


# Report 2 - Run training with alpha values: 0.75, 0.85, 0.95 and plot the difference, that is the difference between
#            the variance of the current Q-matrix and the average variance of the last 10 Q-matrices
#            Change room size between 100 and 1000
#
# ql.max_rooms = 1000
# R = generate_labyrinth(ql.max_rooms, 2)
#
# # for alpha 0.75
# Q = np.zeros(R.shape)
# ql.alpha = 0.75
# ql.gamma = 0.6
# ql.save_diff_per_episodes = True
# ql.train(0, R, Q)
#
# # create first subplot
# episodes = np.arange(0, len(ql.difference_per_episodes), 1)
# plt.subplot(311)
# plt.plot(episodes,ql.difference_per_episodes, '-', lw=0.5, c='red', aa=True)
# plt.title('Episodes by difference (Alpha: 0.75)')
# plt.xlabel('Episodes')
# plt.ylabel('Difference')
#
# #second alpha 0.85
# Q = np.zeros(R.shape)
# alpha = 0.4
# gamma = 0.6
# # create second subplot
# ql.difference_per_episodes = []
# ql.train(0, R, Q)
# episodes = np.arange(0, len(ql.difference_per_episodes), 1)
# plt.subplot(312)
# plt.plot(episodes,ql.difference_per_episodes, '-', lw=0.5, c='red', aa=True)
# plt.title('Episodes by difference (Alpha: 0.4)')
# plt.xlabel('Episodes')
# plt.ylabel('Difference')
#
# # third alpha 0.95
# Q = np.zeros(R.shape)
# alpha = 0.2
# gamma = 0.6
# # create third subplot
# ql.difference_per_episodes = []
# ql.train(0, R, Q)
# episodes = np.arange(0, len(ql.difference_per_episodes), 1)
# plt.subplot(313)
# plt.plot(episodes,ql.difference_per_episodes, '-', lw=0.5, c='red', aa=True)
# plt.title('Episodes by difference (Alpha: 0.2)')
# plt.xlabel('Episodes')
# plt.ylabel('Difference')
#
# plt.show()
#-----------------------------------------------------------------------------------------------------------------------

# Report 3 - Run training and plot the results with 20 gamma values on 100 / 1000 rooms labyrinth

# ql.max_rooms = 1000
# R = generate_labyrinth(ql.max_rooms, 2)
#
# ql.save_episodes_per_training = True
#
# for i in range (1,17):
#     Q = np.zeros(R.shape)
#     ql.gamma = 0.05 * i
#     ql.train(0, R, Q)
#
# #plot performance
# gammas = np.arange(0.05, 0.85, 0.05)                            # Alpha values for Y axis
# plt.plot(ql.num_of_episodes_per_training,
#          gammas, '.', lw=0.5, c='red', aa=True)                 # plot with continuous line, 0.5 wide red line
# plt.yticks(gammas)                                              # put ticks in Y at every alpha value
#
# plt.title('1000 rooms, Alpha = 0.7, Epsilon = 0.9')                                  # set title
# plt.xlabel('Episodes')                                          # set x label
# plt.ylabel('Gamma')                                             # set y label
# plt.show()                                                      # show plot
#-----------------------------------------------------------------------------------------------------------------------


# Report 4 - Run training with gamma values: 0.05, 0.65, 0.95 and plot the difference, that is the difference between
#            the variance of the current Q-matrix and the average variance of the last 10 Q-matrices
#            Change room size between 100 and 1000

# ql.max_rooms = 1000
# R = generate_labyrinth(ql.max_rooms, 2)
#
# # for gamma 0.75
# Q = np.zeros(R.shape)
# ql.gamma = 0.75
# ql.save_diff_per_episodes = True
# ql.train(0, R, Q)
#
# # create first subplot
# episodes = np.arange(0, len(ql.difference_per_episodes), 1)
# plt.subplot(311)
# plt.plot(episodes,ql.difference_per_episodes, '-', lw=0.5, c='red', aa=True)
# plt.title('Episodes by difference (Gamma: 0.75)')
# plt.xlabel('Episodes')
# plt.ylabel('Difference')
#
# #second gamma 0.4
# Q = np.zeros(R.shape)
# gamma = 0.4
# # create second subplot
# ql.difference_per_episodes = []
# ql.train(0, R, Q)
# episodes = np.arange(0, len(ql.difference_per_episodes), 1)
# plt.subplot(312)
# plt.plot(episodes,ql.difference_per_episodes, '-', lw=0.5, c='red', aa=True)
# plt.title('Episodes by difference (Gamma: 0.4)')
# plt.xlabel('Episodes')
# plt.ylabel('Difference')
#
# # third gamma 0.2
# Q = np.zeros(R.shape)
# gamma = 0.2
# # create third subplot
# ql.difference_per_episodes = []
# ql.train(0, R, Q)
# episodes = np.arange(0, len(ql.difference_per_episodes), 1)
# plt.subplot(313)
# plt.plot(episodes,ql.difference_per_episodes, '-', lw=0.5, c='red', aa=True)
# plt.title('Episodes by difference (Gamma: 0.2)')
# plt.xlabel('Episodes')
# plt.ylabel('Difference')
#
# plt.show()
#
#-----------------------------------------------------------------------------------------------------------------------

# Report 5 - Difference with single or double epsilon with 3 different values

#            Change room size between 100 and 1000
# Change ql.double_epsilon to True / False

# ql.max_rooms = 1000
# ql.double_epsilon = True
# R = generate_labyrinth(ql.max_rooms, 2)

# # for epsilon 0.4
# Q = np.zeros(R.shape)
# ql.epsilon = 0.4
# ql.save_diff_per_episodes = True
# ql.train(0, R, Q)

# # create first subplot
# episodes = np.arange(0, len(ql.difference_per_episodes), 1)
# plt.subplot(311)
# plt.plot(episodes,ql.difference_per_episodes, '-', lw=0.5, c='red', aa=True)
# plt.title('Episodes by difference (Epsilon: 0.4)')
# plt.xlabel('Episodes')
# plt.ylabel('Difference')

# #second alpha 0.6
# Q = np.zeros(R.shape)
# gamma = 0.6
# # create second subplot
# ql.difference_per_episodes = []
# ql.train(0, R, Q)
# episodes = np.arange(0, len(ql.difference_per_episodes), 1)
# plt.subplot(312)
# plt.plot(episodes,ql.difference_per_episodes, '-', lw=0.5, c='red', aa=True)
# plt.title('Episodes by difference (Epsilon: 0.6)')
# plt.xlabel('Episodes')
# plt.ylabel('Difference')

# # third alpha 0.8
# Q = np.zeros(R.shape)
# gamma = 0.8
# # create third subplot
# ql.difference_per_episodes = []
# ql.train(0, R, Q)
# episodes = np.arange(0, len(ql.difference_per_episodes), 1)
# plt.subplot(313)
# plt.plot(episodes,ql.difference_per_episodes, '-', lw=0.5, c='red', aa=True)
# plt.title('Episodes by difference (Epsilon: 0.8)')
# plt.xlabel('Episodes')
# plt.ylabel('Difference')

# plt.show()
#-----------------------------------------------------------------------------------------------------------------------

# Report 6 - Plot early stop, difference per episode - 1000 rooms, fixed parameters
#
# ql.max_rooms = 100
# ql.double_epsilon = True
# ql.save_diff_per_episodes = True
# R = generate_labyrinth(ql.max_rooms, 2)
# ql.print_q_matrices = True
#
# # Early stop OFF
# ql.early_stop = False
# Q = np.zeros(R.shape)
# ql.train(0, R, Q)
#
# # create first subplot
# episodes = np.arange(0, len(ql.difference_per_episodes), 1)
# plt.subplot(211)
# plt.plot(episodes,ql.difference_per_episodes, '-', lw=0.5, c='red', aa=True)
# plt.title('Early stop: OFF, 100 rooms, Epsilon: 0.9, Alpha 0.7, Gamma: 0.6')
# plt.xlabel('Episodes')
# plt.ylabel('Difference')

# # early stop ON
# Q = np.zeros(R.shape)
# ql.early_stop = True
#
# # create second subplot
# ql.difference_per_episodes = []
# ql.train(0, R, Q)
# episodes = np.arange(0, len(ql.difference_per_episodes), 1)
# plt.subplot(212)
# plt.plot(episodes,ql.difference_per_episodes, '-', lw=0.5, c='red', aa=True)
# plt.title('Early stop: ON, 100 rooms, Epsilon: 0.9, Alpha 0.7, Gamma: 0.6')
# plt.xlabel('Episodes')
# plt.ylabel('Difference')

#plt.show()
#-----------------------------------------------------------------------------------------------------------------------

# Report 7 - steps per episode, and difference per episode with huge labyrinth

ql.max_rooms = 10000
ql.max_exits = 200
R = generate_labyrinth(ql.max_rooms, ql.max_exits)

# track resets
valid = False
tracker = 0

# for alpha 0.75
Q = np.zeros(R.shape)
ql.early_stop = True
ql.alpha = 0.7
ql.gamma = 0.3
ql.epsilon = 0.7
ql.double_espilon = True
ql.save_diff_per_episodes = True
ql.save_num_of_steps = True
while not valid:
    valid = ql.train(0, R, Q)
    if not valid:
        tracker += 1

# plot difference per episode
episodes = np.arange(0, len(ql.difference_per_episodes), 1)
plt.subplot(211)
#plt.ylim(0, 0.001)
plt.plot(episodes,ql.difference_per_episodes, '-', lw=0.5, c='red', aa=True)
plt.title('Episodes by difference ')
plt.xlabel('Episodes')
plt.ylabel('Difference')

# plot steps per episodes
stps = np.arange(0,len(ql.num_of_steps_per_episode), 1)
plt.subplot(212)
plt.plot(stps, ql.num_of_steps_per_episode,                      # plot with continuous line, 0.5 wide red line
           '-', lw=0.5, c='red', aa=True)                        # put ticks in Y at every 10 between 0:160

plt.title('Steps per episode')                                   # set title
plt.xlabel('Episodes')                                           # set x label
plt.ylabel('Steps')


plt.show()

print('Total rejected labyrinths before training: ' + str(tracker))

numpy.save('d:/10000_rooms_diff_per_episodes.npy',ql.difference_per_episodes)
numpy.save('d:/10000_rooms_steps_per_episodes.npy',ql.num_of_steps_per_episode)
