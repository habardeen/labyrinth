import numpy as np
import math

max_doors = 10       # Max doors to be generated per room
door_ratio = 2      # Absolute maximum doors per room factor (num_rooms / door_ratio)


def generate_labyrinth(num_rooms, num_exits):

    labyrinth = np.full((num_rooms, num_rooms), 0, dtype=int)

    # Create exits
    labyrinth[-1][-1] = 100
    for door in range(num_exits + 1):
        exit_room = np.random.randint(0, num_rooms - 1)
        labyrinth[exit_room][-1] = 100
        labyrinth[-1][exit_room] = 1

    # Create rooms connections
    for room in range(0, num_rooms - 1):
        room_doors = np.count_nonzero(labyrinth[room])
        new_doors = np.random.randint(1, max_doors + 1)

        # Excluding rooms with doors > doors_total
        doors_total = math.ceil(int(num_rooms / door_ratio))
        while room_doors == 0 or room_doors <= new_doors:
            for door in range(new_doors):
                target = np.random.randint(0, num_rooms - 1)
                if np.count_nonzero(labyrinth[target]) <= doors_total:
                    labyrinth[room, target] = 1
                    labyrinth[target, room] = 1
            room_doors = np.count_nonzero(labyrinth[room])

    return labyrinth


def test(num_rooms, num_exits):
    l = generate_labyrinth(num_rooms, num_exits)
    print(l)
